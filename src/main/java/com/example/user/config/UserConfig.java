package com.example.user.config;

import java.beans.PropertyVetoException;  

import javax.sql.DataSource; 
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.context.annotation.Bean;  
import org.springframework.context.annotation.ComponentScan;  
import org.springframework.context.annotation.Configuration;  
import org.springframework.context.annotation.PropertySource;  
import org.springframework.core.env.Environment;  
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;  
  
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer; 

import com.mchange.v2.c3p0.ComboPooledDataSource;


@Configuration  
@EnableWebMvc  
@EnableTransactionManagement  
@ComponentScan("com.example.user")  
@PropertySource(value = { "classpath:application.properties" }) 
public class UserConfig implements WebMvcConfigurer {

	@Autowired  
    private Environment env;  
      
    @Bean  
    public DataSource myDataSource() {  
          
        // create connection pool  
        ComboPooledDataSource myDataSource = new ComboPooledDataSource();  
  
        // set the jdbc driver  
        try {  
            myDataSource.setDriverClass("com.mysql.jdbc.Driver");         
        }  
        catch (PropertyVetoException exc) {  
            throw new RuntimeException(exc);  
        }  
          
        // set database connection props  
        myDataSource.setJdbcUrl(env.getProperty("jdbc.url"));  
        myDataSource.setUser(env.getProperty("jdbc.user"));  
        myDataSource.setPassword(env.getProperty("jdbc.password"));  
          
        // set connection pool props  
        myDataSource.setInitialPoolSize(getIntProperty("connection.pool.initialPoolSize"));  
        myDataSource.setMinPoolSize(getIntProperty("connection.pool.minPoolSize"));  
        myDataSource.setMaxPoolSize(getIntProperty("connection.pool.maxPoolSize"));       
        myDataSource.setMaxIdleTime(getIntProperty("connection.pool.maxIdleTime"));  
  
        return myDataSource;  
    }
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
                .allowedOrigins("http://localhost:4200")
                .allowedMethods("*")
                .maxAge(3600L)
                .allowedHeaders("*")
                .exposedHeaders("Authorization")
                .allowCredentials(true);
			}
		};
	}
      
    // need a helper method   
    // read environment property and convert to int  
      
    private int getIntProperty(String propName) {  
          
        String propVal = env.getProperty(propName);  
          
        // now convert to int  
        int intPropVal = Integer.parseInt(propVal);  
        return intPropVal;  
    }
}
