export class LoginObj {
  username!: string;
  password!: string;
}

export class User {
  id!: BigInt;
  firstname!: string;
  lastname!: string;
  email!: string;
  username!: string;
  password!: string;
  isactive!: boolean;
  roleId!: BigInt;
}

export class UserRole {
  id!: BigInt;
  firstname! : string;
  lastname!: String;
  email!: string;
  username!: string;
  role!: string;
  isactive!: boolean;
}

export class Role {
  id!: BigInt;
  name!: string;
}
