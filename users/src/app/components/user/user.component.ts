import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';    

import { User, Role } from '../../classes/object';
import { UserService } from '../../services/user-service/user.service';
import { RoleService } from '../../services/role-service/role.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  userId!: string | null;
  user = new User();
  roles: Role[] = [];

  constructor(private userService: UserService, private roleService: RoleService, private route : ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {  
      this.userId = params.get('userId');
      if(this.userId != null)
        this.getUser(this.userId);
    });
    if(this.userService.isLoggedIn()) {  
      this.getRoles();
    } else {  
      this.router.navigate(['/login']);  
    }
  }

    // create the form object.  
  form = new FormGroup({
    firstname : new FormControl('' , Validators.required),  
    lastname : new FormControl('' , Validators.required),  
    email : new FormControl('' , Validators.required),  
    password : new FormControl('' , Validators.required),  
    username : new FormControl('' , Validators.required),  
    role : new FormControl('' , Validators.required),  
  });  
  
  UserForm(userInfo: any) {
    this.user.firstname = this.form.controls['firstname'].value;
    this.user.lastname = this.form.controls['lastname'].value;  
    this.user.email = this.form.controls['email'].value;
    this.user.username = this.form.controls['username'].value;  
    this.user.password = this.form.controls['password'].value; 
    this.user.roleId = this.form.controls['role'].value;
    this.userService.saveUser(this.user, 
      (response: any) => {
        console.log("User saved");
      }, (error: any) => {
        console.log("Error saving user", error);
      });  
  }

  getUser(userId: string) {
    this.userService.getUser(userId,
      (response: any) => {
        this.user = response;
      }, (error: any) => {
        console.log("Error getting User", error);
      });
  }

  getRoles() {
    this.roleService.getRoles((response: any) => {
      this.roles = response;
    }, (error: any) => {
      console.log("Error getting Roels", error);
    });
  }

}
