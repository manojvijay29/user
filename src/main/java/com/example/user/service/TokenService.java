package com.example.user.service;

import com.example.user.model.Token;

public interface TokenService {

	public Token updateToken(String email, String authenticationToken, String secretKey);

	public Token getToken(String username);
	
	public String[] createJWT(String id, String issuer, String subject, String role , long ttlMillis);

	public Token saveToken(Long userId, Long roleId, String authenticationToken, String secretKey, String email);
}
