package com.example.user.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.user.dao.RoleDao;
import com.example.user.dao.UserDao;
import com.example.user.dao.UserRoleDao;
import com.example.user.model.User;
import com.example.user.model.Role;
import com.example.user.model.Token;

import com.example.user.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired  
    private UserDao userDao;
	
	@Autowired
	RoleDao roleDao;
	
	@Autowired
	UserRoleDao userRoleDao;

	@Override
	public Long saveUser(User user) {
		user = userDao.save(user);
		return user.getId();
	}

	@Override
	public User userLogin(String userName, String password) {
		Token t = getUserToken(userName, password);
		return userDao.getById(t.getUserId());
	}

	@Override
	public List<User> getUsers() {
		return (List<User>) userDao.findAll();
	}
	
	@Override
	public User saveUserEmail(String email, User user) {
		user.setEmail(email);
		return userDao.save(user);
	}

	@Override
	public User update(Long userId, User user) {
		User updatedUser = userDao.getById(userId);
		if(user.getFirstname() != null)
			updatedUser.setFirstname(user.getFirstname());
		if(user.getLastname() != null)
			updatedUser.setLastname(user.getLastname());
		if(user.getIsactive() != null)
			updatedUser.setIsactive(user.getIsactive());
		return userDao.save(updatedUser);
	}
	

	public Token getUserToken(String userName, String password) {
		User user = userDao.findByUserNameAndPassword(userName, password);
		Role role = roleDao.getById(userRoleDao.findByUserId(user.getId()).getRole().getId());
		Token t = new Token();
		t.setUserID(user.getId());  
        t.setEmailId(user.getEmail());
        t.setRoleId(role.getId());
        return t;
	}

}
