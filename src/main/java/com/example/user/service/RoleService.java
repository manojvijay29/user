package com.example.user.service;

import com.example.user.model.Role;

public interface RoleService {

	public Role getUserRole(Long userId);
}
