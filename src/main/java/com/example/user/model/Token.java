package com.example.user.model;

import javax.persistence.Column;  
import javax.persistence.Entity;  
import javax.persistence.GeneratedValue;  
import javax.persistence.GenerationType;  
import javax.persistence.Id;  
import javax.persistence.Table; 
  
@Entity  
@Table(name="token")  
public class Token {  
      
    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id")  
    private Long tokenId;  
      
    @Column(name="user_id" , unique=true)  
    private Long userId;  
    
    @Column(name="role_id")
    private Long roleId;
       
    @Column(name="authenticationToken")  
    private String authenticationToken;  
      
    @Column(name="secretKey")  
    private String secretKey;  
      
    @Column(name="email_id")  
    private String emailId;

	public Token() {}
  
    public Token(Long userId, Long roleId, String authenticationToken, String secretKey, String email) {
    	super();
    	this.userId = userId;
    	this.roleId = roleId;
    	this.authenticationToken = authenticationToken;
    	this.secretKey = secretKey;
    	this.emailId = email;
	}

	public Long getTokenId() {  
        return tokenId;  
    }  
  
    public void setTokenId(Long tokenId) {  
        this.tokenId = tokenId;  
    }  
  
    public Long getUserId() {  
        return userId;  
    }  
  
    public void setUserID(Long userId) {  
        this.userId = userId;  
    }  
  
    public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getAuthenticationToken() {  
        return authenticationToken;  
    }  
  
    public void setAuthenticationToken(String authenticationToken) {  
        this.authenticationToken = authenticationToken;  
    }  
  
    public String getSecretKey() {  
        return secretKey;  
    }  
  
    public void setSecretKey(String secretKey) {  
        this.secretKey = secretKey;  
    }  
  
    public String getEmailId() {  
        return emailId;  
    }  
  
    public void setEmailId(String emailId) {  
        this.emailId = emailId;  
    }
}  
