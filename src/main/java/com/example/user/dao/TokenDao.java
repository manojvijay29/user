package com.example.user.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.user.model.Token;

public interface TokenDao extends JpaRepository<Token, Long> {
  
	@Transactional
	@Query(value = "select * from token where email_Id = :userEmail", nativeQuery = true)
    public Token getToken(@Param("userEmail") String email);
  
	@Transactional
	@Query(value = "select * from token where user_id = :userId and authenticationToken = :token", nativeQuery = true)
    public Token tokenAuthentication(@Param("token") String token,@Param("userId")  Long userId);
	
	@Transactional
	@Query(value = "select * from token where email_Id = :email", nativeQuery = true)
	public Token findByEmail(@Param("email") String email);
	
}
