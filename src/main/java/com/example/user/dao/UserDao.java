package com.example.user.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.user.model.User;

public interface UserDao extends JpaRepository<User, Long> {
	
	@Transactional
	@Query(value = "select * from user where id = :id", nativeQuery = true)
	public User findById(@Param("id") String id);
	
	@Transactional
	@Query(value = "select * from user where username = :username and password = :password and isactive IS TRUE", nativeQuery = true)
	public User findByUserNameAndPassword(@Param("username") String username, @Param("password") String password);

	@Transactional
	@Query(value = "select * from user where username = :username and isactive IS TRUE", nativeQuery = true)
	public User findByUserName(@Param("username") String username);
	
}
