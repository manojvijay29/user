import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
    
    baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }

    getRoles(successFn: { (response: any): void }, errorFn: { (error: any): void }) {
        let url = this.baseUrl + "/role/all";

        return this.http.get(url).subscribe(response => {
            successFn(response)
        }, error => {
            errorFn(error);
        });
    }

    getRole(roleId: any, successFn: { (response: any): void }, errorFn: { (error: any): void }) {
        let url = this.baseUrl + "/role?roleId=" + roleId;
        return this.http.get(url).subscribe(response => {
            successFn(response)
        }, error => {
            errorFn(error);
        });
    }
}