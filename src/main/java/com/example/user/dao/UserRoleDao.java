package com.example.user.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.user.model.UserRole;

public interface UserRoleDao extends JpaRepository<UserRole, Long> {

	@Transactional
	@Query(value = "select * from user_roles where user_id = :userId", nativeQuery = true)
	public UserRole findByUserId(@Param("userId") Long userId);
	
	@Transactional
	@Query(value = "select * from user_roles where role_id = :roleId", nativeQuery = true)
	public UserRole findByRoleId(@Param("roleId") Long roleId);
}
