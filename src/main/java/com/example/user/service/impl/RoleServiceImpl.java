package com.example.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.user.dao.RoleDao;
import com.example.user.dao.UserRoleDao;
import com.example.user.model.Role;
import com.example.user.model.UserRole;
import com.example.user.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	RoleDao roleDao;
	
	@Autowired
	UserRoleDao userRoleDao;

	@Override
	public Role getUserRole(Long userId) {
		UserRole userRole = userRoleDao.findByUserId(userId);
		return roleDao.getById(userRole.getRole().getId());
	}

}
