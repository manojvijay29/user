package com.example.user.controller;

import java.util.ArrayList;
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;  
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.user.dao.RoleDao;
import com.example.user.dao.UserDao;
import com.example.user.dao.UserRoleDao;
import com.example.user.model.Role;
import com.example.user.model.Token;
import com.example.user.model.User;
import com.example.user.model.UserRole;
import com.example.user.model.impl.UserTokenData;
import com.example.user.model.impl.UsersData;
import com.example.user.service.RoleService;
import com.example.user.service.TokenService;
import com.example.user.service.UserService;

@RestController  
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200/", allowedHeaders = "*", exposedHeaders = "Authorization")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	UserRoleDao userRoleDao;

	@Autowired
	RoleDao roleDao;
	
	@PostMapping("/login")
    public UserTokenData login(@RequestBody User user) {
		UserTokenData userData = new UserTokenData();
        // Authenticate User.
        user = userService.userLogin(user.getUsername(), user.getPassword());
        Role role = roleService.getUserRole(user.getId());
          
        /* 
         * If User is authenticated then Do Authorization Task. 
         */  
        if (user.getIsactive()) {  
            /* 
             * Generate token. 
             */  
            String tokenData[] = tokenService.createJWT(user.getUsername(), "User", "JWT Token",  
                    role.getName(), 43200000);  
              
            // get Token.  
            String token = tokenData[0];
  
            // Check if token is already exist.  
            Token isUserEmailExists = tokenService.getToken(user.getUsername());
              
            /* 
             * If token exist then update Token else create and insert the token. 
             */  
            if (isUserEmailExists != null) {  
            	isUserEmailExists = tokenService.updateToken(user.getEmail(), token, tokenData[1]);
            } else {  
                userService.saveUserEmail(user.getEmail(), user);
                isUserEmailExists = tokenService.saveToken(user.getId(), role.getId(), token, tokenData[1], user.getEmail());
            }
            userData.setUserId(user.getId());
            userData.setRoleId(role.getId());
            userData.setToken(isUserEmailExists.getAuthenticationToken());
            return userData;  
        } 
        // if not authenticated return  status what we get.  
        else {  
            return userData;
        } 
    }
	
	@GetMapping("/user/all")
	public List<UsersData> getAllUsers() {
		List<User> users = userDao.findAll();
		List<UsersData> usersData = new ArrayList<>();
		for(User user: users) {
			UsersData userData = new UsersData();
			userData.setFirstname(user.getFirstname());
			userData.setLastname(user.getLastname());
			userData.setUsername(user.getUsername());
			userData.setEmail(user.getEmail());
			UserRole userRole = userRoleDao.findByUserId(user.getId());
			userData.setRoleId(userRole.getRole().getId());
			usersData.add(userData);
		}
		return usersData;
	}
	
	@GetMapping("/user")
	public User getUser(@RequestParam String userid) {
		return userDao.findById(userid);
	}
	
	@PostMapping("/user/add")
	public Long addUser(@RequestBody UsersData user) throws Exception {
		User existingUser = userDao.findByUserName(user.getUsername());
		if(existingUser == null) {
			User newUser = new User();
			newUser.setFirstname(user.getFirstname());
			newUser.setLastname(user.getLastname());
			newUser.setEmail(user.getEmail());
			newUser.setPassword(user.getPassword());
			newUser.setIsactive(true);
			Long userId = userService.saveUser(newUser);
			UserRole userRole = new UserRole();
			userRole.setUser(newUser);
			Role role = roleDao.getById(user.getRoleId());
			userRole.setRole(role);
			userRoleDao.save(userRole);
			return userId;
		} else {
			existingUser.setFirstname(user.getFirstname());
			existingUser.setLastname(user.getLastname());
			existingUser.setEmail(user.getEmail());
			existingUser.setPassword(user.getPassword());
			userService.saveUser(existingUser);
			return existingUser.getId();
		}
	}
	
	@PutMapping("/user/update")
	public User updateUser(@RequestBody User user) {
		return userService.update(user.getId(), user);
	}
	
	@GetMapping("/role/all")
	public List<Role> getAllRoles() {
		return roleDao.findAll();
	}
	
	@GetMapping("/role")
	public Role getRole(@RequestParam String roleId) {
		return roleDao.findById(roleId);
	}

}
