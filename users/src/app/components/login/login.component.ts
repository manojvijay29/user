import { Component, OnInit } from '@angular/core';  
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';  

import { UserService } from '../../services/user-service/user.service';
import { LoginObj } from '../../classes/object';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login = new LoginObj();

  constructor(private userService: UserService, private router : Router) { }

  ngOnInit(): void {
    if(this.userService.isLoggedIn()) {  
      this.router.navigate(['/user' , sessionStorage.getItem('userId')]);  
    } else {  
      this.router.navigate(['/login']);  
    }
  }


  // create the form object.  
  form = new FormGroup({  
    username : new FormControl('' , Validators.required),  
    password : new FormControl('' , Validators.required)  
  });  
  
  Login(loginInfo: any) {
    this.login.username = this.form.controls['username'].value;;
    this.login.password = this.form.controls['password'].value;
    this.userService.login(this.login, (response: any) => {

      sessionStorage.setItem("token" , response.token);
      sessionStorage.setItem("userId", response.userId);
      sessionStorage.setItem("roleId", response.roleId)

      this.router.navigate(['/user', response.userId]);
    }, (error: any) => {
      console.log("Error in authentication", error);
    }); 
  }
}
