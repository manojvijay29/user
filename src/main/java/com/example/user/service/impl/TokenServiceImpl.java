package com.example.user.service.impl;

import java.security.Key;
import java.util.Date;
import java.util.Random;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.user.dao.TokenDao;
import com.example.user.model.Token;
import com.example.user.service.TokenService;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenServiceImpl implements TokenService {
	
	@Autowired
	TokenDao tokenDao;
	
	@Override
	public Token updateToken(String email, String authenticationToken, String secretKey) {
		Token token = tokenDao.findByEmail(email);
		token.setAuthenticationToken(authenticationToken);
		token.setSecretKey(secretKey);
		return tokenDao.save(token);
	}
	
	public Token saveToken(Long userId, Long roleId, String authenticationToken, String secretKey, String email) {
		return new Token(userId, roleId, authenticationToken, secretKey, email);
	}

	@Override
	public Token getToken(String username) {
		return tokenDao.getToken(username);
	}
	
	@Override
	public String[] createJWT(String id, String issuer, String subject, String role , long ttlMillis) {  
        
        //The JWT signature algorithm we will be using to sign the token  
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;  
       
        long nowMillis = System.currentTimeMillis();  
        Date now = new Date(nowMillis);  
          
        Random random = new Random();  
        String secretKey = id  + Integer.toString(random.nextInt(1000));  
      
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secretKey);  
          
        Key signingKey = null;  
        try{  
              
            signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());  
        }  
        catch(Exception e)  
        {  
            System.out.println("Exception while generating key " + e.getMessage() );  
        }  
          
        JwtBuilder builder = Jwts.builder().setId(id)  
                                    .setIssuedAt(now)  
                                    .setSubject(subject)  
                                    .setIssuer(issuer)  
                                    .signWith(signatureAlgorithm, signingKey);  
          
        //if it has been specified, let's add the expiration  
        if (ttlMillis >= 0) {  
        long expMillis = nowMillis + ttlMillis;  
            Date exp = new Date(expMillis);  
            builder.setExpiration(exp);  
        }  
          
        String[] tokenInfo = {builder.compact() , secretKey};
        return tokenInfo;  
          
    }

}
