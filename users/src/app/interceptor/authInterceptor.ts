import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";

import { UserService } from '../services/user-service/user.service';
import { environment } from '../../environments/environment';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private userService: UserService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let isLoggedIn = this.userService.isLoggedIn();
        let isLogoutUrl = req.url.startsWith(environment.baseUrl + "/logout");

        const token = sessionStorage.getItem('token');

        if (isLoggedIn && !isLogoutUrl) {
            req = req.clone({
                setHeaders: { Authorization: `Bearer ${token}` }
            });
        }
        return next.handle(req);
    }
}