import { TestBed } from '@angular/core/testing';

import { AuthInterceptor } from './authInterceptor';

describe('AuthInterceptor', () => {
  let intercept: AuthInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    intercept = TestBed.inject(AuthInterceptor);
  });

  it('should be created', () => {
    expect(intercept).toBeTruthy();
  });
});