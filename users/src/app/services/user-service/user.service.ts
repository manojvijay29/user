import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

import { LoginObj, User } from '../../classes/object';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  login(login : LoginObj, successFn: { (response: any): void }, errorFn: { (error: any): void }) {
      let url = this.baseUrl + "/login";
      return this.http.post(url, login).subscribe(response => {
        successFn(response)
      }, error => {
        errorFn(error);
      })
  }

  logout(successFn: { (response: any): void }, errorFn: { (error: any): void }){
    let url = this.baseUrl + "/logout";
    // Remove the token from the localStorage.
    return this.http.post(url, {}).subscribe(response => {
      successFn(response)
    }, error => {
      errorFn(error);
    })
  }

  saveUser(user : User, successFn: { (response: any): void }, errorFn: { (error: any): void }) {
    let url = this.baseUrl + "/user/add";

    return this.http.post(url, user).subscribe(response => {
      successFn(response)
    }, error => {
      errorFn(error);
    });
  }

  getUser(userId: any, successFn: { (response: any): void }, errorFn: { (error: any): void }){
      let url = this.baseUrl + "/user?userid=" + userId;

      return this.http.get(url).subscribe(response => {
        successFn(response)
      }, error => {
        errorFn(error);
      });
  }

  getUsers(successFn: { (response: any): void }, errorFn: { (error: any): void }){
    let url = this.baseUrl + "/user/all";

    return this.http.get(url).subscribe(response => {
      successFn(response)
    }, error => {
      errorFn(error);
    });
  }

  /*
  * Check whether User is loggedIn or not.
  */

  isLoggedIn() {

    // create an instance of JwtHelper class.
    let jwtHelper = new JwtHelperService();

    // get the token from the localStorage as we have to work on this token.
    let token = sessionStorage.getItem('token');

    // check whether if token have something or it is null.
    if(!token) {
      return false;
    }

    // get the Expiration date of the token by calling getTokenExpirationDate(String) method of JwtHelper class. this method accepts a string value which is nothing but a token.

    else {
      let expirationDate = jwtHelper.getTokenExpirationDate(token);

      // check whether the token is expired or not by calling isTokenExpired() method of JwtHelper class.

      let isExpired = jwtHelper.isTokenExpired(token);

      return !isExpired;
    }
  }
}
