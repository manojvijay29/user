package com.example.user.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.user.model.Role;

public interface RoleDao extends JpaRepository<Role, Long> {
	
	@Transactional
	@Query(value = "select * from role where id = :id", nativeQuery = true)
	public Role findById(@Param("id") String id);
}
