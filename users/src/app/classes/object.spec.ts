import { User } from './object';
import { UserRole } from './object';

describe('User', () => {
  it('should create an instance', () => {
    expect(new User()).toBeTruthy();
  });
});

describe('UserRole', () => {
  it('should create an instance', () => {
    expect(new UserRole()).toBeTruthy();
  });
});
