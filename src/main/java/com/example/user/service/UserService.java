package com.example.user.service;

import java.util.List;

import com.example.user.model.User;

public interface UserService {
	
	public Long saveUser(User user);  
    
	public List<User> getUsers();
	
	public User update(Long userId, User user);
    
    public User userLogin(String userName , String password);
    
    public User saveUserEmail(String email, User user);
}
