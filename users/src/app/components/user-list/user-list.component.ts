import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../services/user-service/user.service'; 
import { Role, User, UserRole } from '../../classes/object';
import { RoleService } from '../../services/role-service/role.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  haveData= 0;  
  isAdminUser = false;
  data: UserRole[] = [];
  roles: Role[] = [];

  constructor(private userService: UserService, private roleService : RoleService, private router: Router) { }

  ngOnInit(): void {
    if(this.userService.isLoggedIn()) {
      this.getRoles();  
    } else {  
      this.router.navigate(['/login']);  
    }
  }

  getRoles() {
    this.roleService.getRoles((response: any) => {
      this.roles = response;
      this.getCurrentUserRole();
    }, (error: any) => {
      console.log("Error fetching all roles", error);
    });
  }

  getCurrentUserRole() {
    this.roleService.getRole(sessionStorage.getItem("roleId"),
    (response: any) => {
      if(response.name == "Admin")
        this.isAdminUser = true; 
      this.getUserList();
    }, (error: any) => {
      console.log("Error getting role", error);
    })
  }

  getUserList() {
    this.haveData = 0;
    this.data = [];
    this.userService.getUsers(
      (response: any) => {
        response.forEach((result: User) => {
          let row = new UserRole();
          row.id = result.id;
          row.firstname = result.firstname;
          row.lastname = result.lastname;
          row.email = result.email;
          row.username = result.username;
          row.role = this.roles.filter(role => role.id == result.roleId)[0].name;
          this.data.push(row);
        });
        this.haveData = 1;
      }, (error: any) => {
        console.log("Error fetching all users", error);
      }
    )
  }

  editUser(userId: any) {
    this.router.navigate(["user/" + userId])
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(['']);
    /*this.userService.logout(
      (response: any) => {
      }, (error: any) => {
        console.log("Error Logging out", error);
      }
    );*/
  }

}
